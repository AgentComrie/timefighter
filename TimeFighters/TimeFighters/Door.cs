﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Boulderdash
{
    class Door : Tile
    {

        // ------------------
        // Data
        // ------------------

        private Level ourLevel;
        private bool isOpen = false;
        private Texture2D isClosedTexture;
        private Texture2D isOpenTexture;

        // ------------------
        // Behaviour
        // ------------------

        public Door(Texture2D newIsClosedTexture, Texture2D newIsOpenTexture, Level newLevel)
            : base(newIsClosedTexture)
        {
            ourLevel = newLevel;
            isClosedTexture = newIsClosedTexture;
            isOpenTexture = newIsOpenTexture;
        }
        // ------------------
        public bool TryTouch(Vector2 direction)
        {
           
            Vector2 newGridPos = GetTilePosition() + direction;

            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            // If the door is open and the player collides with it then progress to the next level
            if (tileInDirection != null && tileInDirection is Player && isOpen == true)
            {
                ourLevel.Victory();
            }
            // Ask the level what is on the floor in this direction
            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridPos);

            
            return ourLevel.TryMoveTile(this, newGridPos);
        }
        // ------------------
        public void OpenDoor()
        {
            isOpen = true;
            texture = isOpenTexture;
        }
        // ------------------
        public void CloseDoor()
        {
            isOpen = false;
            texture = isClosedTexture;
        }
        // ------------------
        public bool GetisOpen(bool isOpen)
        {
            return isOpen;
        }
        // ------------------
    }
}

