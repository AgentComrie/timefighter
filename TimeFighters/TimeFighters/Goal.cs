﻿using Microsoft.Xna.Framework.Graphics;

namespace Boulderdash
{
    class Goal : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Goal(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
