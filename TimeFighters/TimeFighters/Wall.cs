﻿using Microsoft.Xna.Framework.Graphics;


namespace Boulderdash
{
    class Wall : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Wall(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
