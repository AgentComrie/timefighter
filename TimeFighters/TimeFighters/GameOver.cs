﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Boulderdash
{
    class GameOver : Screen
    {
        // ------------------
        // Data
        // ------------------
        private Text gameName;
        private Text gameName2;
        private Text startPrompt;
        private Game1 game;


        // ------------------
        // Behaviour
        // ------------------
        public GameOver(Game1 newGame)
        {
            game = newGame;
        }
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("fonts/largeFont");
            SpriteFont smallFont = content.Load<SpriteFont>("fonts/mainFont");

            gameName = new Text(titleFont);
            gameName.SetTextString("Well Done");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.White);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 100));

            gameName2 = new Text(titleFont);
            gameName2.SetTextString("You Beat the game");
            gameName2.SetAlignment(Text.Alignment.CENTRE);
            gameName2.SetColor(Color.White);
            gameName2.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 200));

            startPrompt = new Text(smallFont);
            startPrompt.SetTextString("Press enter to exit game");
            startPrompt.SetAlignment(Text.Alignment.CENTRE);
            startPrompt.SetColor(Color.White);
            startPrompt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 300));



        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            gameName.Draw(spriteBatch);
            gameName2.Draw(spriteBatch);
            startPrompt.Draw(spriteBatch);
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            //Check if the player has pressed enter

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // If the player has pressed enter.....

            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                //TODO: CHANGE TO LEVEL SCREEN
                System.Environment.Exit(0);
            }
        }
        // ------------------
    }
}
