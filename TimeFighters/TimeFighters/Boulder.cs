﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System;


namespace Boulderdash
{
    class Boulder : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        private Vector2 directionD;

        private bool isFalling = true;
        private bool isSliding = false;
        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 0.8f;

        // ------------------
        // Behaviour
        // ------------------
        public Boulder(Texture2D newTexture, Level newLevel)
            : base(newTexture)
        {
            ourLevel = newLevel;
        }
        // ------------------
        public bool TryPush(Vector2 direction)
        {
            // New position the Boulder will be in after the push
            Vector2 newGridPos = GetTilePosition() + direction;

            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            // If the target tile is a wall, we can't move there - return false.
            if (tileInDirection != null && tileInDirection is Boulder)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Dirt)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Diamond)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Floor)
            {
                return false;
            }

            // Ask the level what is on the floor in this direction
            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridPos);

            // Move our tile (Boulder) to the new position
            return ourLevel.TryMoveTile(this, newGridPos);
        }
        // ------------------
        public bool TryFall()
        {
            // New position the boulder will be in after the fall
            Vector2 newGridPosG = GetTilePosition() + new Vector2 (0,+1);
            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPosG);

            if (tileInDirection == null || tileInDirection is Player)
            {
                return ourLevel.TryMoveTile(this, newGridPosG);
            }
            else if (tileInDirection is Boulder || tileInDirection is Diamond)
            {
                bool slideRightSuccess = TrySlidingR();
                if (slideRightSuccess == false)
                {
                    bool slideLeftSuccess = TrySlidingL();
                    if (slideLeftSuccess == true)
                    {
                        return true;
                    }
                }
                if (slideRightSuccess == true)
                {
                    return true;
                }
            }
            return false;
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            // Add to time since we last moved
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frameTime;

            // If there is nothing under the boulder let it fall on a timer
            if (timeSinceLastMove >= MOVE_COOLDOWN)
            {
                TryFall();
                timeSinceLastMove = 0;
            }
        }
        // ------------------        
        public bool TrySlidingR()
        {
            // New position the boulder will be in after the slide
            Vector2 newGridPosG = GetTilePosition() + new Vector2(1, +1);
            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPosG);

            if (tileInDirection == null)
            {
                return ourLevel.TryMoveTile(this, newGridPosG);
            }
            return false;
        }
        // ------------------
        public bool TrySlidingL()
        {
            // New position the boulder will be in after the slide
            Vector2 newGridPosG = GetTilePosition() + new Vector2(-1, +1);
            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPosG);

            if (tileInDirection == null)
            {
                return ourLevel.TryMoveTile(this, newGridPosG);
            }
            return false;
        }
        // ------------------

    }
}
