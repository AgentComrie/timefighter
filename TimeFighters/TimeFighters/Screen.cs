﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Boulderdash
{
    class Screen
    {
        // ------------------
        // Behaviour
        // ------------------
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            // Empty
        }
        // ------------------
        public virtual void Update(GameTime gameTime)
        {
            // Empty
        }
        // ------------------
    }
}
