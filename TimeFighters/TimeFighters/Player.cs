﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Boulderdash
{
    class Player : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        private Boulder ourBoulder;
        private float timeSinceLastMove = 0;

        private const float MOVE_COOLDOWN = 0.2f;

        private int noOfDiamonds = 0;

        // ------------------
        // Behaviour
        // ------------------
        public Player(Texture2D newTexture, Level newLevel)
            : base(newTexture)
        {
            ourLevel = newLevel;
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            // Add to time since we last moved
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            timeSinceLastMove += frameTime;

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;

            // Check each key
            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
                ourLevel.EvaluateVictory();
                
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
                ourLevel.EvaluateVictory();
                
            }
            else if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
                ourLevel.EvaluateVictory();
                
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
                ourLevel.EvaluateVictory();
                
            }

            // If we have pressed any direction, try to move there!
            if (movementInput != Vector2.Zero && timeSinceLastMove >= MOVE_COOLDOWN)
            {
                TryMove(movementInput);
                timeSinceLastMove = 0;
            }
        }
        // ------------------
        private bool TryMove(Vector2 direction)
        {
            Vector2 newGridPos = GetTilePosition() + direction;

            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            // If the target tile is a wall, we can't move there - return false.
            if (tileInDirection != null && tileInDirection is Wall)
            {
                
                return false;
            }

            // If the target tile is a boulder, try to push it.
            if (tileInDirection != null && tileInDirection is Boulder)
            {
                Boulder targetBox = tileInDirection as Boulder;
                bool pushSuccess = targetBox.TryPush(direction);

                if (pushSuccess == false)
                {
                    return false;
                }
            }

            // If the target tile is a diamond, try to collect it.
            if (tileInDirection != null && tileInDirection is Diamond)
            {
                ourLevel.EvaluateVictory();
                
            }

            // If the target tile is a door and open then walk thought it.
            if (tileInDirection != null && tileInDirection is Door)
            {
                ourLevel.EvaluateVictory();
            }


            // We need ask the level if we can move to this new position
            // See if the move was successful.
            bool moveResult = ourLevel.TryMoveTile(this, newGridPos);

            // return true or false based on move successfullness
            return moveResult;
        }
        // ------------------
    }
}
