﻿using Microsoft.Xna.Framework.Graphics;

namespace Boulderdash
{
    class Floor : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Floor(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
