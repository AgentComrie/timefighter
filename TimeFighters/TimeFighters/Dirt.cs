﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Boulderdash
{
    class Dirt : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        private bool isCollide = false;
        private Texture2D nonTouchedTexture;
        private Texture2D touchedTexture;

        // ------------------
        // Behaviour
        // ------------------
        public Dirt(Texture2D newNonTouchedTexture, Texture2D newTouchedTexture, Level newLevel)
            : base(newNonTouchedTexture)
        {
            ourLevel = newLevel;
            nonTouchedTexture = newNonTouchedTexture;
            touchedTexture = newNonTouchedTexture;

        }
        // ------------------

        // ------------------
        public bool TryPush(Vector2 direction)
        {
            // fuction is used for testing if player is colliding with the dirt tile
            Vector2 newGridPos = GetTilePosition() + direction;

            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            if (tileInDirection != null && tileInDirection is Player)
            {
                Collide();
            }

            // Ask the level what is on the floor in this direction
            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridPos);

            
            return ourLevel.TryMoveTile(this, newGridPos);

        }
        // ------------------
        private void Collide()
        {
            isCollide = true;
            texture = touchedTexture;

        }
        // ------------------
    }
}
